# Information / Информация

SPEC-файл для создания RPM-пакета **vsftpd**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/vsftpd`.
2. Установить пакет: `dnf install vsftpd`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)